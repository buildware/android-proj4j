package org.osgeo.proj4j;

import java.util.ArrayList;
import java.util.List;

import org.osgeo.proj4j.datum.Datum;
import org.osgeo.proj4j.datum.Ellipsoid;
import org.osgeo.proj4j.proj.LongLatProjection;
import org.osgeo.proj4j.proj.Projection;
import org.osgeo.proj4j.units.DegreeUnit;
import org.osgeo.proj4j.units.Unit;
import org.osgeo.proj4j.units.Units;

/**
 * Represents a projected or geodetic geospatial coordinate system,
 * to which coordinates may be referenced.
 * A coordinate system is defined by the following things:
 * <ul>
 * <li>an {@link Ellipsoid} specifies how the shape of the Earth is approximated
 * <li>a {@link Datum} provides the mapping from the ellipsoid to
 * actual locations on the earth
 * <li>a {@link Projection} method maps the ellpsoidal surface to a planar space.
 * (The projection method may be null in the case of geodetic coordinate systems).
 * <li>a {@link Unit} indicates how the ordinate values
 * of coordinates are interpreted
 * </ul>
 *
 * @author Martin Davis
 * @see CRSFactory
 */
public class CoordinateReferenceSystem //implements Parcelable
{
    // allows specifying transformations which convert to/from Geographic coordinates on the same datum
    public static final CoordinateReferenceSystem CS_GEO = new CoordinateReferenceSystem(
            "CS_GEO", null, null, null, null, Axis.ENU, new ArrayList<String>(), new ArrayList<String>());

    //TODO: add metadata like authority, id, name, parameter string, datum, ellipsoid, datum shift parameters

    private String name;
    private String[] params;
    private Datum datum;
    private Projection proj;

    // SV
    private String authority = "Undefined";
    private int srid = -1;
    private Unit vunit = null;
    private Axis axis = Axis.ENU;
    private List<String> nadGrids = new ArrayList<>();
    private List<String> geoidGrids = new ArrayList<>();

    public CoordinateReferenceSystem(String name, String[] params, Datum datum, Projection proj,
                                     Unit vunit, Axis axis, List<String> nadGrids, List<String> geoidGrids) {
        this.name = name;
        this.params = params;
        this.datum = datum;
        this.proj = proj;

        if (name == null) {
            String projName = "null-proj";
            if (proj != null)
                projName = proj.getName();
            this.name = projName + "-CS";
        }

        // SV
        this.vunit = vunit != null ? vunit : Units.METRES;
        this.axis = axis;
        this.nadGrids = nadGrids;
        this.geoidGrids = geoidGrids;
    }

    public String getName() {
        return name;
    }

    public String[] getParameters() {
        return params;
    }

    public Datum getDatum() {
        return datum;
    }

    public Projection getProjection() {
        return proj;
    }

    // SV
    public String getAuthority() {
        return authority;
    }

    // SV
    public void setAuthority(String authority) {
        this.authority = authority;
    }

    // SV
    public int getSRID() {
        return srid;
    }

    // SV
    public void setSRID(int srid) {
        this.srid = srid;
    }

    // SV
    public Unit getVerticalUnits() {
        return this.vunit;
    }

    // SV
    public Axis getAxis() {
        return this.axis;
    }

    // SV
    public List<String> getNadGrids() {
        return this.nadGrids;
    }

    // SV
    public List<String> getGeoidGrids() {
        return this.geoidGrids;
    }

    public String getParameterString() {
        if (params == null) return "";

        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < params.length; i++) {
            buf.append(params[i]);
            buf.append(" ");
        }
        return buf.toString();
    }

    /**
     * creates a geographic (unprojected) {@link coordinatereferencesystem}
     * based on the {@link datum} of this crs.
     * this is useful for defining {@link coordinatetransform}s
     * to and from geographic coordinate systems,
     * where no datum transformation is required.
     * the {@link units} of the geographic crs are set to {@link units#degrees}.
     *
     * @return a geographic coordinatereferencesystem based on the datum of this crs
     */
    public CoordinateReferenceSystem createGeographic() {
        Datum datum = getDatum();
        Projection geoProj = new LongLatProjection();
        geoProj.setEllipsoid(getProjection().getEllipsoid());
        geoProj.setUnits(Units.DEGREES);
        geoProj.initialize();
        return new CoordinateReferenceSystem("GEO-" + datum.getCode(), null, datum, geoProj, null,
                Axis.ENU, new ArrayList<String>(), new ArrayList<String>());
    }

    public String toString() {
        return name;
    }
}
