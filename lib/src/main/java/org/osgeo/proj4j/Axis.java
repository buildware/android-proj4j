package org.osgeo.proj4j;

public class Axis {

	/**
	 * East - North - Up
	 */
	public static final Axis ENU = new Axis("enu");
	
	/**
	 * West - South - Up
	 */
	public static final Axis WSU = new Axis("wsu");
	
	private final String value;
		
	private Axis(String value)
	{
		if (value == null || value.length() != 3)
			throw new IllegalArgumentException("3-character value is expected");
		
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	@Override
	public boolean equals(Object o) {
		return (o instanceof Axis && ((Axis) o).value.toLowerCase().equals(value));
	}

	@Override
	public String toString() {
		return "Axis[" + value + "]";
	}
}
